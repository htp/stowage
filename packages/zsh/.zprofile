# Prepend directories to the path.
# Directories listed later in the array appear earlier in the path.
typeset -a directories=(
  "/opt/homebrew/bin"
  "${HOME}/bin"
)

for directory in "${directories[@]}"; do
  export PATH="${directory}:${PATH}"
done
