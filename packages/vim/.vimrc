" Disable Vi-compatibility mode.
set nocompatible

" Copy the indent from the current line when starting a new line.
set autoindent

" Automatically reload files if they're changed outside of Vim.
set autoread

" Make backspace behave like it does in typical editors.
set backspace=eol,indent,start

" Tie vim's clipboard to macOS' pasteboard if possible.
if has("clipboard")
  set clipboard=unnamed
endif

" Keep swap files in ~/tmp by default; fall back to the same directory
" as the file being edited otherwise.
set directory=~/tmp//,.

" Insert spaces instead of tabs.
set expandtab

" Allow unsaved buffers in the background.
set hidden

" Highlight search matches.
set hlsearch

" Perform case-insensitive searches if the query is completely lowercase,
" and case-sensitive searches otherwise. (See also: smartcase.)
set ignorecase

" Search while typing search commands.
set incsearch

" Show line numbers. (See also: relativenumber.)
set number

" Show line numbers relative to the current line. (See also: number.)
set relativenumber

" Show the cursor line and cursor position.
set ruler

" Indent using two spaces. (See also: softtabstop, tabstop.)
set shiftwidth=2

" Show the command (as it's being typed) on the last line of the screen.
set showcmd

" Perform case-insensitive searches if the query is completely lowercase,
" and case-sensitive searches otherwise. (See also: ignorecase.)
set smartcase

" Indent using two spaces. (See also: shiftwidth, tabstop.)
set softtabstop=2

" Add new splits top-to-bottom and left-to-right. (See also: splitright.)
set splitbelow

" Add new splits top-to-bottom and left-to-right. (See also: splitbelow.)
set splitright

" Indent using two spaces. (See also: shiftwidth, softtabstop.)
set tabstop=2

" Turn on command tab-completion.
set wildmenu
set wildmode=longest:full,full

" Enable file type detection.
filetype plugin indent on

" Enable syntax highlighting.
syntax enable

" Remap the leader key to ','.
let mapleader=","

" Map shortcut for turning off highlighting search matches.
noremap // :nohlsearch<CR>

" Map shortcuts for navigating tabs.
noremap <C-y> :tabprevious<CR>
noremap <C-o> :tabnext<CR>

" Map miscellaneous shortcuts.
nnoremap ! :!
nnoremap <leader>b  :buffer<space>
nnoremap <leader>bd :bdelete<space>
nnoremap <leader>c  :close<CR>
nnoremap <leader>e  :edit<space>
nnoremap <leader>f  :set filetype=
nnoremap <leader>hn :new<space>
nnoremap <leader>hr :vertical resize<space>
nnoremap <leader>hs :split<space>
nnoremap <leader>ls :buffers<CR>
nnoremap <leader>tc :tabclose<CR>
nnoremap <leader>tn :tabnew<space>
nnoremap <leader>vn :vnew<space>
nnoremap <leader>vr :resize<space>
nnoremap <leader>vs :vsplit<space>
nnoremap <leader>w  :write<CR>

" When switching back to a buffer, automatically reload it if its file has changed.
autocmd! FocusGained,BufEnter * checktime

" Turn off syntax highlighting for Markdown files.
autocmd! FileType markdown setlocal syntax=off
