#!/usr/bin/env sh

set -u

STOWAGE_PLATFORM="$(uname -s)"
readonly STOWAGE_PLATFORM

STOWAGE_PATH=""
if [ "${STOWAGE_PLATFORM}" == "Darwin" ]; then
  STOWAGE_PATH="${HOME}/Projects/stowage"
else
  STOWAGE_PATH="${HOME}/projects/stowage"
fi
readonly STOWAGE_PATH

STOWAGE_GIT="$(command -v git)"
readonly STOWAGE_GIT

STOWAGE_STOW="$(command -v stow)"
readonly STOWAGE_STOW


main() {
  ensure_prerequisites
  ensure_stowage

  restow
}

ensure_prerequisites() {
  local issue=""
  local missing=""
  if [ -z "${STOWAGE_GIT}" ] && [ -z "${STOWAGE_STOW}" ]; then
    issue="neither were found"
    missing="Git and Stow"
  elif [ -z "${STOWAGE_GIT}" ]; then
    issue="Git was not found"
    missing="Git"
  elif [ -z "${STOWAGE_STOW}" ]; then
    issue="Stow was not found"
    missing="Stow"
  fi

  if [ -n "${issue}" ]; then
    abort "$(cat <<EOS
Stowage uses Git and Stow to manage configuration files,
but %s on your system.

Please install %s and try again.\n
EOS
)" "${issue}" "${missing}"
  fi
}

ensure_stowage() {
  if ! [ -e "${STOWAGE_PATH}" ]; then
    "${STOWAGE_GIT}" clone https://codeberg.org/htp/stowage.git "${STOWAGE_PATH}"
  elif [ -d "${STOWAGE_PATH}" ]; then
    "${STOWAGE_GIT}" -C "${STOWAGE_PATH}" pull
  fi
}

restow() {
  for package in "${STOWAGE_PATH}"/packages/*; do
    "${STOWAGE_STOW}" --restow --dir "${STOWAGE_PATH}"/packages --target "${HOME}" "${package##*/}"
  done
}

abort() {
  printf "$@" >&2
  exit 1
}

main "$@"
